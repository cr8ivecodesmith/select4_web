from django.shortcuts import render

# Create your views here.

def index(request):
    context = {
        'title': 'Simple Notes',
        'message': 'Hello world!',
    }
    template_name = 'notes/index.html'
    return render(request, template_name, context)
