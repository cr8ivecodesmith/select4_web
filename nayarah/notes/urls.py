from django.urls import path

from nayarah.notes import views

app_name = 'nayarah_notes'
urlpatterns = [
    path('', views.index, name='index'),
]
