SELECT4 Web App Project
=======================

A simple note-taking web app in Django


## Requirements

This application is built and tested on:

- Ubuntu 16.04
- Python 3.6


## Installation

1) Install requirements in a virtualenv

    :::console
    pip install -r requirements.txt


## Usage

To follow
